# Remote Work tools


This is a repository to track useful tools for remote workers. Established and newly-acquainted with this reality of working from home.


## How to add items?

* **Option A:** Open a Merge Request to add your suggestions to the README.md file (use markdown format)
* **Option A:** Open an issue and dump your suggestions in the description. Someone will add them to the README.md

Use format (pick a suitable and funny emoji):

```
* 🛠 [Tool name](http://example.com) - Short description of what it is.
```

---

## 🛠 Work tools

### 🧠 Documentation / Knowledge bases

* 📚 [Notion.so](https://www.notion.so/) - easy way to start documenting your company's process, invaluable when working remotely.

### 📹 Video calls

* 🔭 [Zoom.us](https://zoom.us) - not the prettiest but the most reliable and useful.

### 🕞🕣 Timezone management

* 🌐 [Menu World Time](https://apps.apple.com/us/app/menu-world-time/id1446377255?l=en&mt=12) [macOS]- Menu bar app to show several timezones in a simple and effective way.
* 🌎 [Every Time Zone](http://everytimezone.com/) - Handy to quickly sort out a point in time and share a link with a group spread over several timezones.
---

## 🕺 Social Tools

### 💫 Synchronized Media

* 📺 [Netflix Party](https://www.netflixparty.com/) - Netflix watch parties with your friends, chrome extention 
* 🔊 [jqbx.fm](http://jqbx.fm/) - Listening to the same music while Coworking. Create different rooms with different styles, have multiple DJs (or not).

### 🎯 Remote-friendly games

* 💀 [Cards against Humanity](http://playingcards.io/game/cards-against-humanity) (unofficial) - Play Cards against humanity in a browser
* 🎨️ [skribbl.io](https://skribbl.io) - Play Pictionary together 
* ❓ [WhoCards](http://donatuswolf.com/whocards/questions/index.html) - Who cards for interesting conversation prompts
* 🥳 [Houseparty](https://houseparty.com/) - make informal video calls, with bonus of having party games you can play straight from your phone. (iOS, Android, macOS and Chrome extension)
* 🕵️‍♀️ [Codewords](http://codewordsgame.com/) - like [codenames](https://www.youtube.com/watch?v=zQVHkl8oQEU) but online.

### 📹 Social Video calls

* 🥳 [Houseparty](https://houseparty.com/) - make informal video calls, with bonus of having party games you can play straight from your phone. (iOS, Android, macOS and Chrome extension)

---
